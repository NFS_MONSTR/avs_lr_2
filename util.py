import os
import sys
from PyQt5 import QtGui

from config import *


def resource_path(relative_path):
    """ Get absolute path to resource, works for dev and for PyInstaller """
    #base_path = getattr(sys, '_MEIPASS', os.path.dirname(os.path.abspath(__file__)))
    #return os.path.join(base_path, relative_path)
    return relative_path


def get_pixmap(path):
    return QtGui.QPixmap(resource_path(IMG_PREFIX + path))


def get_piano_pixmap(piano_key):
    return get_pixmap(PIANO_PREFIX + str(piano_key.value) + PNG_SUFFIX)


def get_piano_key_from_freq(freq):
    try:
        freq = PianoFreq(freq)
    except Exception:
        freq = PianoFreq.NONE
    finally:
        return piano_freq_key[freq]


def get_piano_freq_pixmap(freq):
    return get_piano_pixmap(get_piano_key_from_freq(freq))


def get_note_pixmap(piano_key, duration):
    if duration == DURATION_1_4:
        duration_suffix = '3'
    elif duration == DURATION_1_2:
        duration_suffix = '2'
    else:
        duration_suffix = '1'
    if piano_key == PianoKey.DO or piano_key == PianoKey.DO_DIEZ:
        type_suffix = '2'
    else:
        type_suffix = '1'
    if piano_key in DIEZ_KEYS:
        diez_suffix = 'diez'
    else:
        diez_suffix = ''
    return get_pixmap(NOTE_PREFIX + duration_suffix + type_suffix + diez_suffix + PNG_SUFFIX)
