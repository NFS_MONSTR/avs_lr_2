from PyQt5 import QtCore, QtGui, QtWidgets

from util import resource_path

BASE_WINDOW_NAME = 'Proc2'


class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(542, 509)
        self.sendButton = QtWidgets.QPushButton(Form)
        self.sendButton.setGeometry(QtCore.QRect(40, 460, 81, 41))
        self.sendButton.setObjectName("sendButton")
        self.getButton = QtWidgets.QPushButton(Form)
        self.getButton.setGeometry(QtCore.QRect(130, 460, 81, 41))
        self.getButton.setObjectName("getButton")
        self.readButton = QtWidgets.QPushButton(Form)
        self.readButton.setGeometry(QtCore.QRect(220, 460, 91, 41))
        self.readButton.setObjectName("readButton")
        self.writeButton = QtWidgets.QPushButton(Form)
        self.writeButton.setGeometry(QtCore.QRect(320, 460, 91, 41))
        self.writeButton.setObjectName("writeButton")
        self.notesBg = QtWidgets.QLabel(Form)
        self.notesBg.setGeometry(QtCore.QRect(40, 300, 501, 111))
        self.notesBg.setText("")
        self.notesBg.setPixmap(QtGui.QPixmap(resource_path("img/forNotes.jpg")))
        self.notesBg.setObjectName("notesBg")
        self.notesList = []
        for x in range(10):
            note = QtWidgets.QLabel(Form)
            note.setGeometry(QtCore.QRect(110 + 40 * x, 320, 20, 40))
            note.setText("")
            note.setPixmap(QtGui.QPixmap(resource_path("img/Note11.png")))
            note.setObjectName("note_" + str(x))
            note.hide()
            self.notesList.append(note)
        self.clearButton = QtWidgets.QPushButton(Form)
        self.clearButton.setGeometry(QtCore.QRect(420, 460, 81, 41))
        self.clearButton.setObjectName("clearButton")

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)
        self.form = Form

    def change_name(self, name):
        self.form.setWindowTitle(BASE_WINDOW_NAME + ' ' + name)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", BASE_WINDOW_NAME))
        self.sendButton.setText(_translate("Form", "Передача"))
        self.getButton.setText(_translate("Form", "Прием"))
        self.readButton.setText(_translate("Form", "Чтение"))
        self.writeButton.setText(_translate("Form", "Запись"))
        self.clearButton.setText(_translate("Form", "Очистить"))
