import windows2
from reader import Reader
from PyQt5 import QtWidgets
import sys


class ExampleApp(QtWidgets.QMainWindow, windows2.Ui_Form):  # Mine Window class
    global WR, threadpool

    def __init__(self):
        super().__init__()
        self.setupUi(self)

    def attach_buttons(self, read):
        self.sendButton.clicked.connect(read.send)
        self.getButton.clicked.connect(read.receive)
        self.readButton.clicked.connect(fileDialRead)
        self.writeButton.clicked.connect(fileDialWrite)
        self.clearButton.clicked.connect(read.clear)


def fileDialRead():
    fname = QtWidgets.QFileDialog.getOpenFileName()
    read.load_from_file(fname[0])


def fileDialWrite():
    fname = QtWidgets.QFileDialog.getSaveFileName()
    read.write_to_file(fname[0])


def main():
    global window, read
    app = QtWidgets.QApplication(sys.argv)  # Новый экземпляр QApplication
    window = ExampleApp()  # Создаём объект класса ExampleApp
    window.show()  # Показываем окно
    read = Reader(window, isSaver=True)
    window.attach_buttons(read)
    read.start()
    app.exec_()  # и запускаем приложение
    read.exit()


if __name__ == '__main__':  # Если мы запускаем файл напрямую, а не импортируем
    main()  # то запускаем функцию main()
