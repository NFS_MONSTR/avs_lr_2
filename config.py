from enum import Enum

IMG_PREFIX = 'img/'
PIANO_PREFIX = 'Piano'
NOTE_PREFIX = 'Note'
PNG_SUFFIX = '.png'

FILENAME = {
    False: 'img/p1.txt',
    True: 'img/p2.txt'
}

DURATION_1_4 = 250
DURATION_1_2 = 500
DURATION_1 = 1000


class PianoKey(Enum):
    NONE = 0
    DO = 1
    DO_DIEZ = 2
    RE = 3
    RE_DIEZ = 4
    MI = 5
    FA = 6
    FA_DIEZ = 7
    SOL = 8
    SOL_DIEZ = 9
    LA = 10
    LA_DIEZ = 11
    SI = 12


class PianoFreq(Enum):
    NONE = 0
    DO = 523
    DO_DIEZ = 555
    RE = 587
    RE_DIEZ = 623
    MI = 659
    FA = 699
    FA_DIEZ = 741
    SOL = 784
    SOL_DIEZ = 832
    LA = 880
    LA_DIEZ = 934
    SI = 988


class State(Enum):
    NONE = 0
    PLAY = 0x1
    CREATING = 0x2
    SENDING = 0x4
    RECEIVING = 0x8
    STOP = 0x16
    EXIT = 0x32


state_names = {
    State.NONE: '',
    State.PLAY: 'Воспроизведение',
    State.CREATING: 'Создание мелодии',
    State.SENDING: 'Передача',
    State.RECEIVING: 'Прием',
    State.STOP: 'Стоп',
    State.EXIT: 'Выход'
}

piano_freq_key = {
    PianoFreq.NONE: PianoKey.NONE,
    PianoFreq.DO: PianoKey.DO,
    PianoFreq.DO_DIEZ: PianoKey.DO_DIEZ,
    PianoFreq.RE: PianoKey.RE,
    PianoFreq.RE_DIEZ: PianoKey.RE_DIEZ,
    PianoFreq.MI: PianoKey.MI,
    PianoFreq.FA: PianoKey.FA,
    PianoFreq.FA_DIEZ: PianoKey.FA_DIEZ,
    PianoFreq.SOL: PianoKey.SOL,
    PianoFreq.SOL_DIEZ: PianoKey.SOL_DIEZ,
    PianoFreq.LA: PianoKey.LA,
    PianoFreq.LA_DIEZ: PianoKey.LA_DIEZ,
    PianoFreq.SI: PianoKey.SI,
}

piano_key_freq = {
    PianoKey.NONE: PianoFreq.NONE,
    PianoKey.DO: PianoFreq.DO,
    PianoKey.DO_DIEZ: PianoFreq.DO_DIEZ,
    PianoKey.RE: PianoFreq.RE,
    PianoKey.RE_DIEZ: PianoFreq.RE_DIEZ,
    PianoKey.MI: PianoFreq.MI,
    PianoKey.FA: PianoFreq.FA,
    PianoKey.FA_DIEZ: PianoFreq.FA_DIEZ,
    PianoKey.SOL: PianoFreq.SOL,
    PianoKey.SOL_DIEZ: PianoFreq.SOL_DIEZ,
    PianoKey.LA: PianoFreq.LA,
    PianoKey.LA_DIEZ: PianoFreq.LA_DIEZ,
    PianoKey.SI: PianoFreq.SI,
}

CHECK_KEYS_LIST = ['A', 'W', 'S', 'E', 'D', 'F', 'R', 'G', 'T', 'H', 'Y', 'J']

piano_button_key = {
    'A': PianoKey.DO,
    'W': PianoKey.DO_DIEZ,
    'S': PianoKey.RE,
    'E': PianoKey.RE_DIEZ,
    'D': PianoKey.MI,
    'F': PianoKey.FA,
    'R': PianoKey.FA_DIEZ,
    'G': PianoKey.SOL,
    'T': PianoKey.SOL_DIEZ,
    'H': PianoKey.LA,
    'Y': PianoKey.LA_DIEZ,
    'J': PianoKey.SI,
}

DIEZ_KEYS = [PianoKey.DO_DIEZ, PianoKey.RE_DIEZ, PianoKey.FA_DIEZ, PianoKey.SOL_DIEZ, PianoKey.LA_DIEZ]

NOTE_HEIGHT = {
    PianoKey.DO: 360,
    PianoKey.DO_DIEZ: 360,
    PianoKey.RE: 351,
    PianoKey.RE_DIEZ: 351,
    PianoKey.MI: 345,
    PianoKey.FA: 339,
    PianoKey.FA_DIEZ: 339,
    PianoKey.SOL: 333,
    PianoKey.SOL_DIEZ: 333,
    PianoKey.LA: 327,
    PianoKey.LA_DIEZ: 327,
    PianoKey.SI: 321,
}
