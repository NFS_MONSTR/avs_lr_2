import windows1
from reader import Reader
from PyQt5 import QtWidgets
import sys
from threading import Thread


class ExampleApp(QtWidgets.QMainWindow, windows1.Ui_Form, Thread):  # Mine Window class
    global WR, threadpool

    def __init__(self):
        super().__init__()
        Thread.__init__(self)
        self.setupUi(self)

    def run(self):
        while True:
            app.exec_()

    def attach_buttons(self, read):
        self.createButton.clicked.connect(read.create)
        self.stopButton.clicked.connect(read.stop)
        self.clearButton.clicked.connect(read.clear)
        self.playButton.clicked.connect(read.play)
        self.sendButton.clicked.connect(read.send)
        self.getButton.clicked.connect(read.receive)


def main():
    global read, app
    app = QtWidgets.QApplication(sys.argv)  # Новый экземпляр QApplication
    window = ExampleApp()  # Создаём объект класса ExampleApp
    window.show()  # Показываем окно
    read = Reader(window)
    window.attach_buttons(read)
    read.start()

    app.exec_()

    read.exit()


if __name__ == '__main__':  # Если мы запускаем файл напрямую, а не импортируем
    main()  # то запускаем функцию main()
