from os import remove
from os.path import isfile
from threading import Thread
from time import sleep
from winsound import Beep
import keyboard
from PyQt5 import QtCore
from util import *


class Reader(Thread):
    def __init__(self, window, isSaver=False):
        Thread.__init__(self)
        self.state = State.NONE
        self.counter = 0
        self.thread = None
        self.window = window
        self.melody = []
        self.isSaver = isSaver

    def set_state(self, state):
        self.state = state
        self.window.change_name(state_names[state])

    def duration(self):
        if self.window.duration_1.isChecked():
            return DURATION_1
        if self.window.duration_1_2.isChecked():
            return DURATION_1_2
        if self.window.duration_1_4.isChecked():
            return DURATION_1_4

    def clear(self):
        for x in range(0, self.counter):
            self.window.notesList[x].hide()
        self.melody.clear()
        self.counter = 0
        self.set_state(State.NONE)

    def _play(self):
        for x in range(0, self.counter):
            if self.state != State.PLAY:
                self.window.cursor.setGeometry(QtCore.QRect(110, 400, 251, 50))
                return
            d = self.melody[x][1]
            freq = self.melody[x][0]
            self.window.cursor.setGeometry(QtCore.QRect(110 + 40 * x, 400, 251, 50))
            self.window.pianoKeys.setPixmap(get_piano_freq_pixmap(freq))
            Beep(freq, d)
            self.window.pianoKeys.setPixmap(get_piano_freq_pixmap(PianoFreq.NONE))
        self.window.cursor.setGeometry(QtCore.QRect(110, 400, 251, 50))
        self.set_state(State.NONE)

    def play(self):
        self.set_state(State.PLAY)

    def updateCounter(self):
        self.counter += 1

    def stop(self):
        self.set_state(State.STOP)

    def exit(self):
        self.set_state(State.EXIT)

    def create(self):
        self.set_state(State.CREATING)

    def _create(self):
        if self.counter >= 10:
            return
        for key in CHECK_KEYS_LIST:
            if keyboard.is_pressed(key):
                if self.counter >= 10:
                    return
                d = self.duration()
                piano_key = piano_button_key[key]
                self.window.pianoKeys.setPixmap(get_piano_pixmap(piano_key))
                self.window.notesList[self.counter].setPixmap(get_note_pixmap(piano_key, d))
                self.window.notesList[self.counter].setGeometry(
                    QtCore.QRect(self.window.notesList[self.counter].geometry().left(), NOTE_HEIGHT[piano_key],
                                 51,
                                 41))
                self.window.notesList[self.counter].show()
                self.updateCounter()
                Beep(piano_key_freq[piano_key].value, d)
                self.melody.append((piano_key_freq[piano_key].value, d))
                self.window.pianoKeys.setPixmap(get_piano_pixmap(PianoKey.NONE))
                break

    def write_to_file(self, name):
        f = open(name, "w")
        for x in self.melody:
            f.write(str(x[0]) + " " + str(x[1]) + '\n')
        f.close()

    def _send_to_file(self):
        name = resource_path(FILENAME[self.isSaver])
        self.set_state(State.SENDING)
        self.write_to_file(name)
        while self.state == State.SENDING:
            if not isfile(name):
                self.set_state(State.NONE)
                break
            sleep(0.01)
        try:
            remove(name)
        except Exception:
            pass

    def load_from_file(self, name):
        try:
            if not isfile(name):
                return False
            f = open(name)
        except Exception:
            return False
        self.clear()
        for x in f:
            frq_time = x.split()
            frq = int(float(frq_time[0]))
            d = int(float(frq_time[1]))
            self.melody.append((int(float(frq_time[0])), int(float(frq_time[1]))))
            piano_key = get_piano_key_from_freq(frq)
            self.window.notesList[self.counter].setPixmap(get_note_pixmap(piano_key, d))
            self.window.notesList[self.counter].setGeometry(
                QtCore.QRect(self.window.notesList[self.counter].geometry().left(), NOTE_HEIGHT[piano_key], 51, 41))
            self.window.notesList[self.counter].show()
            self.updateCounter()
        f.close()
        return True

    def _get_from_file(self):
        name = resource_path(FILENAME[not self.isSaver])
        self.set_state(State.RECEIVING)
        while self.state == State.RECEIVING:
            if self.load_from_file(name):
                remove(name)
                self.set_state(State.NONE)
                break
            sleep(0.01)

    def send(self):
        self.set_state(State.SENDING)

    def receive(self):
        self.set_state(State.RECEIVING)

    def run(self):
        while self.state != State.EXIT:
            if self.state == State.PLAY:
                self._play()
            elif self.state == State.STOP:
                self.set_state(State.NONE)
            elif self.state == State.CREATING:
                self._create()
            elif self.state == State.SENDING:
                self._send_to_file()
            elif self.state == State.RECEIVING:
                self._get_from_file()
            sleep(0.01)
